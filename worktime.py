#!/usr/bin/python
import sys
import datetime
from sys import platform
import subprocess
import ctypes

#Help
def help():
    print("Usage: ./worktime.py [ OPTIONS ] [ FILENAME ]\nwhere OPTIONS:= help  - to show this message\n                begin - to write beggining time of work\n                end   - to write ending time of work\n                count - to count time spent working")

#Begin
def begin():
   with open(filename, "a") as workfile:
       workfile.write("begin " + str(datetime.datetime.now().replace(microsecond=0)) + "\n")

   if platform == "linux" or platform == "linux2":
      subprocess.call("notify-send 'Worktime " + filename + "' 'beginned' --icon=dialog-information" , shell=True)
   elif platform == "Windows" or platform == "win32" or platform == "win64":
      ctypes.windll.user32.MessageBoxW(0, "Worktime " + filename + " beginned", "Worktime", 0)
 
#End
def end():
   with open(filename, "a") as workfile:
       workfile.write("end " + str(datetime.datetime.now().replace(microsecond=0)) + "\n")

#Count
def count():
   with open(filename, "r") as workfile:
       lines = workfile.readlines()

   begintime = None
   endtime = None
   note = ""
   difference = datetime.timedelta(seconds=0)

   for line in lines:
       if line.split()[0] == "begin":

           if endtime != None and begintime != None:
               difference += (endtime-begintime)
               begintime = None
               endtime = None

           if begintime == None:
               begintime = datetime.datetime.strptime(line.split()[1] + " " + line.split()[2], "%Y-%m-%d %H:%M:%S")

       elif line.split()[0] == "end":
           endtime = datetime.datetime.strptime(line.split()[1] + " " + line.split()[2], "%Y-%m-%d %H:%M:%S")

   if begintime != None and endtime !=None:
       difference += (endtime-begintime) 
       begintime = None
       endtime = None

   if begintime != None and endtime == None:
       difference += (datetime.datetime.now().replace(microsecond=0)-begintime)
       note = "Until now"
   
   days = "\nWhich is:       " + str(round(int(str(difference).split(":")[0])/8 + int(str(difference).split(":")[1])/480 + int(str(difference).split(":")[1])/28800, 4)) + " Days" 
   hours = "\nWhich is:       " + str(round(int(str(difference).split(":")[0])/8 + int(str(difference).split(":")[1])/480 + int(str(difference).split(":")[1])/28800, 4)*8) + " Hours" 
   output = "Difference is:  " + str(difference) + hours + "\n" + note

   print(output)
   
   if platform == "linux" or platform == "Linux" :
      subprocess.call("notify-send 'Worktime " + filename + "' '" + output + "' --icon=dialog-information" , shell=True)
   elif platform == "Windows" or platform == "win32" or platform == "win64":
      ctypes.windll.user32.MessageBoxW(0, "Worktime " + filename + "\n" + output, "Worktime", 0)
       
if len(sys.argv) < 2 or len(sys.argv) > 3 or sys.argv[1] == "help" or sys.argv[1] == "--help":
    help()

else:
    try:
        filename = sys.argv[2]
    except:
        filename = "worktime"

    if sys.argv[1] == "begin":
        begin()
    elif sys.argv[1] == "end":
        end()
        count()
    elif sys.argv[1] == "count":
        count()
    else:
        help()

