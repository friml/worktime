# Worktime
Python script for counting time spent working.

## Usage

```
Usage: ./worktime.py [ OPTIONS ] [ FILENAME ]
where OPTIONS:= help  - to show this message
                begin - to write beggining time of work
                end   - to write ending time of work
                count - to count time spent working
```

